stages:
  - prepare and lint
  - build deps images
  - build final images
  - tag

variables:
  # bump this if you want to force rebuilding the deps images;
  # they get rebuilt automatically when touching files under `compose/` or `requirements/`
  # or `package.json` or `yarn.lock`
  DEPS_STAMP: '20220207-0'

prepare:
  stage: prepare and lint
  image:
    name: alpine/git
    entrypoint: [""]
  script:
    - GIT_VERSION=$(git log -n1 --pretty='%h')
    - TAG=${GIT_VERSION}-${CI_PIPELINE_ID}
    - DEPS_FRONTEND_COMMIT_HASH=$(git rev-list -1 HEAD -- Dockerfile package.json yarn.lock)
    - DEPS_FRONTEND_COMMIT_COUNT=$(git rev-list --count "$DEPS_FRONTEND_COMMIT_HASH")
    - DEPS_FRONTEND_TAG="deps-${DEPS_STAMP}-${DEPS_FRONTEND_COMMIT_COUNT}-${DEPS_FRONTEND_COMMIT_HASH}-${CI_COMMIT_REF_SLUG}"
    - echo "TAG=$TAG" | tee -a build-env.txt
    - echo "DEPS_FRONTEND_TAG=$DEPS_FRONTEND_TAG" | tee -a build-env.txt
  artifacts:
    reports:
      dotenv: build-env.txt

lint-javascript:
  stage: prepare and lint
  image: debian:bullseye-slim
  before_script:
    - apt update && apt install -y --no-install-recommends
        yarnpkg
  script:
    - yarnpkg
    - yarnpkg lint

lint-dockerfile:
  stage: prepare and lint
  image: hadolint/hadolint:latest-debian
  script:
    - hadolint --ignore DL3008 --ignore DL3006 Dockerfile

.build-docker-image:
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  variables:
    CONTEXT: .
    ARGS: ''
  script:
    - TAG="${JOB_TAG:-${TAG}}"
    - mkdir -p /kaniko/.docker/
    # makes use of Gitlab's Predefined environment variables, see:
    # https://docs.gitlab.com/ee/ci/variables/predefined_variables.html
    - echo "CI_REGISTRY = $CI_REGISTRY"
    - echo "CI_REGISTRY_USER = $CI_REGISTRY_USER"
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    - mkdir /kaniko/tmp/
    - echo "FROM $REPO:$TAG" > /kaniko/tmp/Dockerfile
    - |
      if /kaniko/executor --context=/kaniko/tmp --verbosity=panic --no-push
      then
        echo "⏩ Image $REPO:$TAG already exists, skip rebuilding it"
        exit 0
      else
        echo "👷 Image $REPO:$TAG not found, build it"
      fi
    - set -x;
      /kaniko/executor
        --context "$CONTEXT"
        --dockerfile "$DOCKERFILE"
        --destination "$REPO:$TAG"
        $ARGS
        --skip-unused-stages;
      set +x
    - echo "✅ Pushed $REPO:$TAG"

build-docker-image-deps-frontend:
  extends: .build-docker-image
  stage: build deps images
  variables:
    DOCKERFILE: ./Dockerfile
    REPO: $CI_REGISTRY_IMAGE/deps-frontend
    JOB_TAG: $DEPS_FRONTEND_TAG
    ARGS:
      --target deps-frontend-base

build-docker-image-frontend:
  extends: .build-docker-image
  stage: build final images
  variables:
    DOCKERFILE: ./Dockerfile
    REPO: $CI_REGISTRY_IMAGE/frontend
    ARGS:
      --target frontend
      --build-arg FRONTEND_BASE_IMAGE=$CI_REGISTRY_IMAGE/deps-frontend:$DEPS_FRONTEND_TAG

tag-latest-docker-image:
  stage: tag
  image: debian:bullseye-slim
  before_script:
    - apt update && apt install -y --no-install-recommends
        skopeo
        openssh-client
        ca-certificates
  variables:
    AUTH: $CI_REGISTRY_USER:$CI_REGISTRY_PASSWORD
    TAG_NEW: latest
  script:
    - |
      for I in frontend
      do
        IMAGE="${CI_REGISTRY_IMAGE}/${I}"
        echo Tagging "${IMAGE}:${TAG}"
        skopeo copy --src-creds "$AUTH" --dest-creds "$AUTH" docker://"${IMAGE}:${TAG}" docker://"${IMAGE}:${TAG_NEW}"
        echo 🏷️ Tagged "${IMAGE}:${TAG_NEW}"
      done
  rules:
    - if: '$CI_MERGE_REQUEST_ID'
      when: never
    - if: '$CI_DEFAULT_BRANCH == $CI_COMMIT_BRANCH'
      when: on_success
    - when: never
