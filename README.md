# webapp2: Basic webapp with vuejs 3 + bootstrap 5

## Demo

![webapp demo](public/demo.gif "Web app demo")

## Features

- skeleton created with vue-cli 4.5 using Vue 3 / babel / eslint presets

- SPA (single-page-application) with dynamic routing, see [/src/router/index.js](/src/router/index.js)

- reusable components, see [components dir](/src/components)

- [bootstrap5 can be tweaked using SCSS](https://getbootstrap.com/docs/5.0/customize/sass/#modify-map), see `<style></style>` section in `src/App.vue`.

## Docker set-up

Download the pre-built image and start the service with this command:

    docker run --rm -p 8080:80 registry.gitlab.com/simevo/webapp2/frontend:latest

then visit the app at: http://localhost:8080.

Alternatively, build the docker image locally with:

    docker build . -t webapp2

and run the locally built image with:

    docker run --rm -it -p 8080:80 webapp2

## Kubernetes set-up

Install [minikube](https://minikube.sigs.k8s.io/docs/start/) and [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/#install-using-native-package-management).

Download this repo:

    git clone git@gitlab.com:simevo/webapp2.git
    cd webapp2

Deploy to minikube:

    kubectl apply -f deployment.yaml

[Export the service from minikube](https://minikube.sigs.k8s.io/docs/commands/service/):

    minikube service --url webapp2

this will return something like `http://192.168.39.204:32080`. You can directly access that url on the host where minikube is running; if you are on a different host you must forward the port with:

    ssh -L 8080:192.168.39.204:31360 example.com
    
then you will be able to access the service at http://localhost:8080.

To deploy a separate instance on anoher pod, duplicate `deployment.yaml`, rename service + deployment and deploy the new application:

    cp deployment.yaml deployment1.yaml
    sed -i 's/name: webapp2/name: webapp2_1/g' deployment1.yaml

When you're done, tear down the deployments:

    kubectl delete -f deployment.yaml
    kubectl delete -f deployment1.yaml

## Local install

### Prerequisites

Debian 11 (bullseye):
```
sudo apt install yarnpkg
```

MacOS:

```
brew update
brew install yarn
```

Windows:

Install [Node.js](https://nodejs.org/en/download/)
Be sure to add Windows PATH

Then install node dependencies with:

```
yarnpkg install
```

On Windows install node modules:

```
npm install 
```

### Compiles and hot-reloads for development
```
yarnpkg serve
```

Initialize server on Windows:
```
npm run serve 
```

Open the webapp at http://localhost:8080

### Compiles and minifies for production
```
yarnpkg build
```

The static files to install on the production webserver will be under `/dist`.

### Lints and fixes files
```
yarnpkg lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
