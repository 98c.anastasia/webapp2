import { createWebHistory, createRouter } from 'vue-router'

const routes = [
  {
    path: '/',
    name: 'home',
    component: () => import('../components/HelloWorld.vue'),
    props: { msg: 'Welcome to Your Vue.js App' }
  },
  {
    path: '/table',
    name: 'table',
    component: () => import('../components/Table.vue')
  },
  {
    path: '/form',
    name: 'form',
    component: () => import('../components/MyForm.vue')
  },
  {
    path: '/tabs',
    name: 'tabs',
    component: () => import('../components/Tabs.vue')
  },
  {
    path: '/libpf',
    name: 'libpf',
    component: () => import('../components/Libpf.vue')
  },
  {
    path: '/accordion',
    name: 'accordion',
    component: () => import('../components/Accordion.vue')
  },
  {
    path: '/sankey',
    name: 'sankey',
    component: () => import('../components/Sankey.vue')
  },
  {
    path: '/leaflet',
    name: 'leaflet',
    component: () => import('../components/Leaflet.vue')
  }
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

export default router
